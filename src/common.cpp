#include "common.hpp"

static bool verbose = false;

void set_verbose() {
  verbose = true;
}

void clr_verbose() {
  verbose = false;
}

void generic_variadic_print(FILE* fp, string fmt, va_list args) {
  vfprintf(fp, fmt.c_str(), args);
  fflush(fp);
}

void info(string fmt...) {
  va_list args;
  va_start(args, fmt);
  generic_variadic_print(stdout, fmt, args);
  va_end(args);
}

void vinfo(string fmt...) {
  if (!verbose) return;
  va_list args;
  va_start(args, fmt);
  generic_variadic_print(stdout, fmt, args);
  va_end(args);
}

void warn(string fmt...) {
  va_list args;
  va_start(args, fmt);
  generic_variadic_print(stderr, fmt, args);
  va_end(args);
}

void error(string fmt...) {
  va_list args;
  va_start(args, fmt);
  generic_variadic_print(stderr, fmt, args);
  va_end(args);
  exit(1);
}

string trim_ws(string str) {
  size_t begin = str.find_first_not_of(" \n\r");
  size_t end = str.find_last_not_of(" \n\r");
  if (begin == string::npos) return "";
  return str.substr(begin, end - begin + 1);
}

bool mad_type_is_primitive(mad_type_t type) {
  if (mad_type_is_scalar_primitive(type)) return true;
  switch (type) {
    case mad_type::STR:
      return true;
    default:
      break;
  }
  return false;
}

bool mad_type_is_scalar_primitive(mad_type_t type) {
  switch (type) {
    case mad_type::FLT:
    case mad_type::INT:
      return true;
    default:
      break;
  }
  return false;
}

string mad_type_to_str(mad_type_t type) {
  switch (type) {
    case mad_type::FLT:
      return string("FLT");
    case mad_type::INT:
      return string("INT");
    case mad_type::STR:
      return string("STR");
    case mad_type::CALC:
      return string("CALC");
    default:
      break;
  }
  return string("NONE");
}

mad_type_t str_to_mad_type(string str) {
  if (str == string("FLT")) return mad_type::FLT;
  if (str == string("INT")) return mad_type::INT;
  if (str == string("STR")) return mad_type::STR;
  if (str == string("CALC")) return mad_type::CALC;
  return mad_type::NONE;
}
