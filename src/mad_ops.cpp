#include <math.h>

#include "mad_ops.hpp"

using std::stod;
using std::stol;

mad_ops::mad_ops(mad_vars_t* ext_vars) : rvars(ext_vars) {
  if (!ext_vars) {
    fprintf(stderr, "Error: NULL ext_vars passed to mad_ops constructor\n");
    exit(EXIT_FAILURE);
  }
  //register all ops
  glbl_op_map.insert({string("help"), &mad_ops::help});
  glbl_op_map.insert({string("assign"), &mad_ops::assign});
  glbl_op_map.insert({string("fassign"), &mad_ops::fassign});
  glbl_op_map.insert({string("deassign"), &mad_ops::deassign});
  glbl_op_map.insert({string("listvars"), &mad_ops::listvars});
  glbl_op_map.insert({string("printvar"), &mad_ops::printvar});
  var_op_map.insert({string("concat"), &concat});
  var_op_map.insert({string("sum"), &sum});
  var_op_map.insert({string("difference"), &difference});
  var_op_map.insert({string("product"), &product});
  var_op_map.insert({string("quotient"), &quotient});
  var_op_map.insert({string("remainder"), &remainder});
  var_op_map.insert({string("exponent"), &exponent});
  var_op_map.insert({string("logarithm"), &logarithm});
}

mad_status_t mad_ops::process_line(string glbl_op, string args) {
  auto query = glbl_op_map.find(glbl_op);
  if (query == glbl_op_map.end()) {
    fprintf(stderr, "Error: gbl_op \"%s\" doesn't exist\n", glbl_op.c_str());
    fprintf(stderr, "Please run \"help\" to get a list of ops!\n");
    return EXIT_FAILURE;
  }
  glbl_op_t func = query->second;
  mad_status_t ret = (this->*func)(args);
  return ret;
}

mad_status_t mad_ops::help(string args) {
  if (args.length() != 0) {
    fprintf(stderr, "Error: \"%s\" with arguments isn't yet supported\n",
        __func__);
    return EXIT_FAILURE;
  }
  printf("Op list:\n==========\n");
  printf("help [OP] - print details about OP, or list all OPS if OP unincluded\n");
  printf("assign <VAR_STR> <TYPE_STR> <VALUE> - assign a variable\n");
  printf("fassign (same args as assign) - assign that allows overwriting existing variables\n");
  printf("deassign <VAR_STR> - attempts to delete a variable\n");
  printf("listvars [FILTER] - list all existing variables, if FILTER then\n");
  printf("\t\tonly display variables that have FILTER as substring\n");
  printf("printvar <VAR_STR> - print all information about a variable\n");
  printf("==========\n");
  return EXIT_SUCCESS;
}

// FIXME: currently ignores statement altogether and simply creates a default
// init value variable based on the provided type
// FIXME: need type checking before attempting to convert string to anything
// else
mad_status_t mad_ops::apply_val(mad_var_t* rvar, mad_type_t type,
    string args) {
  if (type == mad_type::NONE || args.length() == 0) {
    rvar->set(type);
    return EXIT_SUCCESS;
  }
  // FIXME: loop through args to parse and evaluate them
  // e.g: even an INT can be set to x + y - 7 * 3, it will just be saved as the
  // value (whereas a CALC would save the equation for example)
  mad_val_t rval;
  string val_str;
  string targs = args; // temp variable that we can modify without changing orig
  size_t val_str_len = targs.find_first_of(' ');
  if (val_str_len != string::npos) {
    if (type != mad_type::CALC) fprintf(stderr, "Warning: currently only supports one value (no variables), others are ignored\n");
    val_str = targs.substr(0, val_str_len);
  } else {
    val_str = targs;
  }
  switch(type) { // just to get variable declaration working
    case mad_type::FLT:
      {
        rval = (mad_FLT_t) stod(val_str);

      }
      break;
    case mad_type::INT:
      {
        rval = (mad_INT_t) stol(val_str);
      }
      break;
    case mad_type::STR:
      {
        rval = (mad_STR_t) val_str;
      }
      break;
    case mad_type::CALC:
      {
        rval = mad_CALC_t(this, rvars, targs);
      }
      break;
    default:
      fprintf(stderr, "Error: Cannot create variable of type %s\n",
          mad_type_to_str(type).c_str());
      return EXIT_FAILURE;
  }
  if (rvar->set(type, rval) != EXIT_SUCCESS) return EXIT_FAILURE;
  return EXIT_SUCCESS;
}

// args should be in the form (needs space-delimiter-based parsing):
// string var_name, string type_str, <STATEMENT>
// TODO: figure out what <STATEMENT> ought to look like
// for example, it can be a single string or a concatenation of strings or a
// single or few values combined with scalar operations (whether the value is
// computed or the operations saved as a CALC depend on whether type is CALC)
mad_status_t mad_ops::generic_assign(string args, bool force) {
  mad_var_t r;
  mad_status_t ret_status;
  string targs = args; // temp variable that we can modify without changing orig
  if (targs.length() == 0) {
    fprintf(stderr, "Error: missing name in args provided to \"%s\": \"%s\"\n",
        __func__, args.c_str());
    return EXIT_FAILURE;
  }
  string name;
  string type_str;
  string statement = string("");
  size_t name_len = targs.find_first_of(' ');
  size_t type_len;
  if (name_len == string::npos) {
    fprintf(stderr, "Error: missing type_str in args provided to \"%s\": \"%s\"\n",
        __func__, args.c_str());
    return EXIT_FAILURE;
  }
  name = targs.substr(0, name_len);
  targs = trim_ws(targs.substr(name_len + 1, targs.size() - name_len - 1));

  type_len = targs.find_first_of(' ');
  if (type_len != string::npos) {
    type_str = targs.substr(0, type_len);
    targs = trim_ws(targs.substr(type_len + 1, targs.size() - type_len - 1));
    statement = targs;
  } else {
    type_str = targs.substr(0, targs.length());
  }
  ret_status = apply_val(&r, str_to_mad_type(type_str), statement);
  if (ret_status != EXIT_SUCCESS) {
    fprintf(stderr, "Error: \"%s\" unable to apply statement to var \"%s\": \"%s\"\n",
        __func__, name.c_str(), statement.c_str());
    return ret_status;
  }
  ret_status = rvars->ins_var(name, r, force);
  if (ret_status != EXIT_SUCCESS) {
    fprintf(stderr, "Error: \"%s\" failed due to being unable to insert var \"%s\" into DB\n",
        __func__, name.c_str());
    return ret_status;
  }
  return EXIT_SUCCESS;
}

mad_status_t mad_ops::assign(string args) {
  return generic_assign(args, false);
}


mad_status_t mad_ops::fassign(string args) {
  return generic_assign(args, true);
}

//args should be in the form:
//string var_name
mad_status_t mad_ops::deassign(string args) {
  if (!args.length()) {
    fprintf(stderr, "Error: \"%s\" failed due to empty args\n", __func__);
    return EXIT_FAILURE;
  }
  size_t name_len = args.find_first_of(' ');
  string name = args.substr(0, name_len);
  mad_status_t ret_status = rvars->rm_var(name);
  if (ret_status != EXIT_SUCCESS) {
    fprintf(stderr, "Error: \"%s\" failed due to being unable to remove var \"%s\" from the DB\n",
        __func__, name.c_str());
    return ret_status;
  }
  return EXIT_SUCCESS;
}

// TODO
mad_status_t mad_ops::listvars(string args) {
  if (!args.length()) return rvars->print_vars(string(""));
  string name;
  string targs = args; // temp variable that we can modify without changing orig
  size_t name_len = targs.find_first_of(' ');
  if (name_len != string::npos) name = targs.substr(0, name_len);
  else name = targs;
  return rvars->print_vars(name);
}

// TODO
mad_status_t mad_ops::printvar(string args) {
  if (args == string("")) {} // currently unused
  return EXIT_FAILURE;
}

mad_status_t mad_ops::var_op_query(string var_op) {
  auto query = var_op_map.find(var_op);
  if (query == var_op_map.end()) return EXIT_FAILURE;
  return EXIT_SUCCESS;
}

mad_status_t mad_ops::exec(string var_op, mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  auto query = var_op_map.find(var_op);
  if (query == var_op_map.end()) {
    fprintf(stderr, "Error: var_op \"%s\" doesn't exist\n", var_op.c_str());
    return EXIT_FAILURE;
  }
  mad_status_t ret = query->second(rvar, x, y);
  return ret;
}

mad_status_t concat(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  if (x->get_type() == y->get_type()) {
    mad_val_t val_x;
    x->get_prim_val(&val_x, x->get_type());
    mad_val_t val_y;
    y->get_prim_val(&val_y, y->get_type());
    switch(x->get_type()) { // either works since they are the same
      case mad_type::STR:
        if (rvar->set(mad_type::STR, get<mad_STR_t>(val_x) + get<mad_STR_t>(val_y))
            != EXIT_SUCCESS) return EXIT_FAILURE;
        break;
      default:
        fprintf(stderr, "Incorrect types provided to %s: %s,%s",
            __func__, mad_type_to_str(x->get_type()).c_str(), mad_type_to_str(y->get_type()).c_str());
        return EXIT_FAILURE;
    }
  } else {
    fprintf(stderr, "Incorrect types provided to %s: %s,%s",
        __func__,
        mad_type_to_str(x->get_type()).c_str(), mad_type_to_str(y->get_type()).c_str());
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_status_t generic_scalar_op(mad_var_t* rvar, mad_var_t* x, mad_var_t* y,
    string var_op) {
  mad_val_t val_x, val_y;
  mad_status_t ret_status;

  /* FIXME: need strings for vars in DB to properly do math with CALC types
  if (x->get_type() == mad_type::CALC && rvar != y) {
    if (rvar == x) {
      return get<mad_CALC_t>(rvar->get_val()).append({y}, {});
    }
    if (!rvar->set(x->get_type(), x->get_val()) != EXIT_SUCCESS)
      return EXIT_FAILURE;
    
    return EXIT_SUCCESS;
  } else if (y->get_type() == mad_type::CALC) {
    if (rvar == y) {

    }
  }
  */

  //FIXME handle CALC types above
  // each must be either a scalar primitive or a CALC
  if ((!mad_type_is_scalar_primitive(x->get_type()) &&
      x->get_type() != mad_type::CALC) ||
      (!mad_type_is_scalar_primitive(y->get_type()) &&
      y->get_type() != mad_type::CALC)) {
    fprintf(stderr, "Incorrect types provided to %s: %s,%s\n",
        var_op.c_str(),
        mad_type_to_str(x->get_type()).c_str(),
        mad_type_to_str(y->get_type()).c_str());
    return EXIT_FAILURE;
  }
  // CALC types handled by conversion to FLT/INT as appropriate
  // prefer to convert CALC to FLT (if both are CALC or one CALC with one FLT)
  if (x->get_type() == mad_type::FLT || y->get_type() == mad_type::FLT ||
      (x->get_type() == mad_type::CALC && y->get_type() == mad_type::CALC)) {
    ret_status = x->get_prim_val(&val_x, mad_type::FLT);
    if (ret_status != EXIT_SUCCESS) return EXIT_FAILURE;
    ret_status = y->get_prim_val(&val_y, mad_type::FLT);
    if (ret_status != EXIT_SUCCESS) return EXIT_FAILURE;
    rvar->set(mad_type::FLT);
  } else {
    // by default convert to INT since we know the other isn't a FLT
    ret_status = x->get_prim_val(&val_x, mad_type::INT);
    if (ret_status != EXIT_SUCCESS) return EXIT_FAILURE;
    ret_status = y->get_prim_val(&val_y, mad_type::INT);
    if (ret_status != EXIT_SUCCESS) return EXIT_FAILURE;
    rvar->set(mad_type::INT);
  }
  //TODO: is there any way to simplify this code?
  switch(rvar->get_type()) {
    case mad_type::FLT:
      if (var_op == string("sum")) {
        ret_status = rvar->set(mad_type::FLT, (mad_val_t)(get<mad_FLT_t>(val_x) + get<mad_FLT_t>(val_y)));
      } else if (var_op == string("difference")) {
        ret_status = rvar->set(mad_type::FLT, (mad_val_t) (get<mad_FLT_t>(val_x) - get<mad_FLT_t>(val_y)));
      } else if (var_op == string("product")) {
        ret_status = rvar->set(mad_type::FLT, (mad_val_t) (get<mad_FLT_t>(val_x) * get<mad_FLT_t>(val_y)));
      } else if (var_op == string("quotient")) {
        if (!get<mad_FLT_t>(val_y)) {
          fprintf(stderr, "Error: cannot perform division by zero\n");
          return EXIT_FAILURE;
        }
        ret_status = rvar->set(mad_type::FLT, (mad_val_t) (get<mad_FLT_t>(val_x) / get<mad_FLT_t>(val_y)));
      } else if (var_op == string("remainder")) {
        fprintf(stderr, "Error: remainder can not be computed with type FLT\n");
        return EXIT_FAILURE;
      } else if (var_op == string("exponent")) {
        ret_status = rvar->set(mad_type::FLT, (mad_val_t) (pow(get<mad_FLT_t>(val_x), get<mad_FLT_t>(val_y))));
      } else if (var_op == string("logarithm")) {
        ret_status = rvar->set(mad_type::FLT, (mad_val_t) (log(get<mad_FLT_t>(val_y)) / log(get<mad_FLT_t>(val_x))));
      }
      break;
    case mad_type::INT:
      if (var_op == string("sum")) {
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (get<mad_INT_t>(val_x) + get<mad_INT_t>(val_y)));
      } else if (var_op == string("difference")) {
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (get<mad_INT_t>(val_x) - get<mad_INT_t>(val_y)));
      } else if (var_op == string("product")) {
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (get<mad_INT_t>(val_x) * get<mad_INT_t>(val_y)));
      } else if (var_op == string("quotient")) {
        if (!get<mad_INT_t>(val_y)) {
          fprintf(stderr, "Error: cannot perform division by zero\n");
          return EXIT_FAILURE;
        }
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (get<mad_INT_t>(val_x) / get<mad_INT_t>(val_y)));
      } else if (var_op == string("remainder")) {
        if (!get<mad_INT_t>(val_y)) {
          fprintf(stderr, "Error: cannot compute remainder of division by zero\n");
          return EXIT_FAILURE;
        }
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (get<mad_INT_t>(val_x) % get<mad_INT_t>(val_y)));
      } else if (var_op == string("exponent")) {
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (pow(get<mad_INT_t>(val_x), get<mad_INT_t>(val_y))));
      } else if (var_op == string("logarithm")) {
        ret_status = rvar->set(mad_type::INT, (mad_val_t) (log(get<mad_INT_t>(val_x)) / log(get<mad_INT_t>(val_y))));
      }
      break;
    default: // this shouldn't happen
      return EXIT_FAILURE;
  }
  return ret_status;
}

mad_status_t sum(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t difference(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t product(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t quotient(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t remainder(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t exponent(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}

mad_status_t logarithm(mad_var_t* rvar, mad_var_t* x, mad_var_t* y) {
  return generic_scalar_op(rvar, x, y, string(__func__));
}
