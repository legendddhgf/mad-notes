#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cstdint>
#include <cstdbool>
#include <pwd.h>
#include <unistd.h>
#include <sys/types.h>

#include "common.hpp"
#include "mad_vars.hpp"
#include "mad_ops.hpp"

using std::string;

bool quit;

/* input pre-gathered line <line> and line strlen <len> (not the actual buffer
 * length)
 *
 * return 0 if command not found (along with warning) or command successful, and
 * error code otherwise
 */
int32_t process_line(string line, mad_ops_t* rops) {
  if (!line.length()) return EXIT_SUCCESS; //ignore lines with no content
  string op;
  string args;

  // TODO: handle all kinds of whitespace
  size_t op_len = line.find_first_of(' ');
  if (op_len == string::npos) {
    op = line;
    args = "";
  } else {
    op = line.substr(0, op_len);
    args = trim_ws(line.substr(op_len + 1, line.size() - op_len - 1));
  }
  if (op == string("quit")) {
    quit = true;
    return EXIT_SUCCESS;
  }
  if (rops->process_line(op, args) != EXIT_SUCCESS) return EXIT_FAILURE;
  return EXIT_SUCCESS;
}

void display_prompt(char* user_name, const char* exe_name) {
  //printf("\33[2K\r");
  info("\n%s@%s $ ", user_name, exe_name);
}

void usage(const char* exe_name) {
  fprintf(stderr, "Usage: %s [-uv]\n", exe_name);
  fprintf(stderr, "-u: ");
}

int main (int argc, char* const* argv) {
  int32_t ret; // generic return variable
  char c;
  char line_in[MAX_LINE_LEN];
  char* line_in_ptr = &line_in[0];
  uint32_t line_in_len;
  string line_in_str;
  struct passwd* pw;
  uid_t uid;
  char user_name[SMALL_BUF_LEN];
  char* user_name_ptr = &user_name[0];
  uint32_t user_name_len = 0;
  mad_vars_t vars;
  mad_ops_t ops(&vars);

  quit = false;

  // zero buffers
  bzero(line_in_ptr, MAX_LINE_LEN);
  bzero(user_name_ptr, SMALL_BUF_LEN);

  while ((c = getopt(argc, argv, "u:v")) != -1) {
    switch(c) {
      case 'u':
        snprintf(user_name_ptr, SMALL_BUF_LEN, "%s", optarg);
        user_name_len = strlen(user_name_ptr);
        break;
      case 'v':
        info("Running in verbose mode...\n\n");
        set_verbose();
        break;
      default:
        warn("Invalid argument: -%c\n", c);
        usage("mad_notes");
        return EXIT_FAILURE;
    }
  }

  if (!user_name_len) {
    // get and set username for session
    uid = geteuid();
    pw = getpwuid(uid);
    if (!pw) {
      snprintf(user_name_ptr, SMALL_BUF_LEN, "unknown_user");
    } else {
      snprintf(user_name_ptr, SMALL_BUF_LEN, "%s", pw->pw_name);
    }
    user_name_len = strlen(user_name_ptr);
  }

  // process line by line user/script input
  // TODO: script input
  do {

    display_prompt(user_name_ptr, "mad_notes");

    if (!fgets(line_in_ptr, MAX_LINE_LEN, stdin)) {
      // TODO: if debug is enabled, print that there was an error processing the
      // last line of input
      continue;
    }
    line_in_len = strlen(line_in_ptr);
    if (line_in_len >= MAX_LINE_LEN - 1) {
      // TODO: introduce error(), info() and related
      fprintf(stderr, "Error: line must be fewer than %u characters\n",
          MAX_LINE_LEN - 1);
      continue;
    }
    line_in_str = string(line_in_ptr, line_in_len);
    ret = process_line(trim_ws(line_in_str), &ops);
    if (ret) {
      // TODO: depending on verbosity, display accordingly
      fprintf(stderr, ">>>Command execution error %d<<<\n", ret);
      continue;
    }

    //TODO: potential line post processing (updating data structures/flushing to
    //storage/etc)
  } while(!quit);

  // FIXME: save all data to storage/any pre-quit actions/etc
  fprintf(stderr, "Quit Successfully\n");

  return EXIT_SUCCESS;
}
