#ifndef MAD_CALC_HPP
#define MAD_CALC_HPP

#include <string>
#include <vector>

#include "common.hpp"

using std::string;
using std::vector;

class mad_ops;
class mad_vars;
class mad_var;

// uses key names for variables and key names for operations to eventually
// perform every operation needed at time of request (by performing lookups for
// the actual variables and function pointers of the actual operations needed)
//
// assert vars.size() == ops.size() + 1
//
// processed in the form of:
// var.val = vars[0].val
// var.val ops[0]= vars[1].val
// ...
// var.val ops[ops.size() - 1]= vars[vars.size() - 1].val;
// return var
typedef class mad_calc {
  private:
    vector<string> op_names;
    vector<string> var_names;
    class mad_ops* rops;
    class mad_vars* rvars;
  public:
    mad_calc(class mad_ops* rops, class mad_vars* rvars,
        vector<string> op_names, vector<string> var_names);
    mad_calc(class mad_ops* rops = NULL, class mad_vars* rvars = NULL,
        string args = string(""));
    mad_status_t init(vector<string> op_names, vector<string> var_names);
    mad_status_t append(vector<string> op_names, vector<string> var_names);
    //TODO:
    // parse any mathematical statement and construct vars and ops with the keys
    // to retrieve the variables (mad_var_t) and operations (function pointers)
    // so the expression can be evaluated at any time.
    // Furthermore, insert constants in to the mad_vars_t in such a way that:
    // -if another constant with the same value is ever referenced, it can be
    // found and we can avoid having to be re-added as a separate entry in DB
    // -it can be easily referenced by other variables
    // --e.g: inserting 5 can be ("CONST_MAD_INT_5", 5)
    // ---in other words, having a key of "CONST_MAD_" + mad_type_to_str(type) +
    //    to_string(VAL) for the insertion of the particular constant to the DB
    mad_status_t init(string args);
    //TODO:
    // this is where this var is evaluated and returned as a single primitive var
    // (but not overwritten)
    // pass in &var to be overwritten as well as the requested return type
    // If you request a string, you will get the intended result, but you can't do
    // math with it anymore
    // If you request a INT or FLT, you will end up with a single value of that
    // type which you can do math with (but any strings would be ignored)
    // No other types are accepted for req_type (only FLT, INT, STR)
    // default primitive scalar value is 0, default string is ""
    mad_status_t eval(class mad_var* rvar, mad_type_t req_type = mad_type::FLT);
} mad_calc_t;

typedef mad_calc_t mad_CALC_t; // maintain naming scheme of other mad_*_t types

// resolve circular dependencies by putting after mad_CALC_t defs
#include "mad_vars.hpp"
#include "mad_ops.hpp"

#endif//MAD_CALC_HPP
