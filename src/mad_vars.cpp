#include <iostream>

#include "mad_ops.hpp"
#include "mad_vars.hpp"

using std::to_string;

mad_var::mad_var() {
  type = mad_type::NONE;
}

mad_var::mad_var(mad_type_t init_type, mad_val_t init_val) {
  type = mad_type::NONE;
  if (set(init_type, init_val) != EXIT_SUCCESS) {
    fprintf(stderr, "Warning: var wasn't initialized correctly!\n");
  }
}

mad_var::mad_var(mad_type_t init_type, mad_val_t* pinit_val) {
  type = mad_type::NONE;
  if (set(init_type, pinit_val) != EXIT_SUCCESS) {
    fprintf(stderr, "Warning: var wasn't initialized correctly!\n");
  }
}

mad_status_t mad_var::set(mad_type_t set_type, mad_val_t set_val) {
  return set(set_type, &set_val);
}

mad_status_t mad_var::set(mad_type_t set_type, mad_val_t* pset_val) {
  if (set_type == mad_type::NONE && this->get_type() == mad_type::NONE) {
    fprintf(stderr, "Error: must provide a type to a variable that doesn't yet have one\n");
    return EXIT_FAILURE;
  }
  // we act as if mad_var only was provided a type if this condition succeeds
  if (!pset_val) {
    switch(set_type) {
      case mad_type::FLT:
        val = (mad_FLT_t) 0;
        break;
      case mad_type::INT:
        val = (mad_INT_t) 0;
        break;
      case mad_type::STR:
        val = (mad_STR_t) string("");
        break;
      case mad_type::CALC: {
        mad_CALC_t temp;
        val = temp;
        break;
                           }
      default:
        fprintf(stderr, "Error: must provide val when setting to NONE type\n");
        return EXIT_FAILURE;
    }
  } else {
    val = *pset_val;
  }
  type = set_type;
  mad_type_t val_type = mad_val_to_mad_type(&val);
  if (val_type != type) {
    fprintf(stderr, "Error: val has wrong type:: e: %s, a: %s\n",
        mad_type_to_str(type).c_str(), mad_type_to_str(val_type).c_str());
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_status_t mad_var::convert(mad_var_t* rvar, mad_type_t req_type) {
  if (type == mad_type::CALC) return get<mad_CALC_t>(val).eval(rvar, req_type);
  if (type == req_type) { // no conversion occurred, just copy to rvar
    rvar->set(this->get_type(), this->get_val());
    return EXIT_SUCCESS;
  }
  if (type == mad_type::STR) {
    fprintf(stderr, "Error: can't convert from STR to non-STR\n");
    return EXIT_FAILURE;
  }
  if(req_type == mad_type::FLT) {
    // assume this->get_type() is INT at this point
    if (this->get_type() != mad_type::INT) {
      fprintf(stderr, "Error: prim conversion to INT can only be from FLT\n");
      return EXIT_FAILURE;
    }
    rvar->set(mad_type::FLT, (mad_FLT_t) get<mad_INT_t>(val));
  } else if (req_type == mad_type::INT) {
    // assume this->get_type() is FLT at this point
    if (this->get_type() != mad_type::FLT) {
      fprintf(stderr, "Error: prim conversion to INT can only be from FLT\n");
      return EXIT_FAILURE;
    }
    rvar->set(mad_type::INT, (mad_INT_t) get<mad_FLT_t>(val));
  } else if (req_type == mad_type::STR) {
    if(this->get_type() == mad_type::FLT) {
      rvar->set(mad_type::STR, (mad_STR_t) to_string((mad_FLT_t) get<mad_FLT_t>(val)));
    } else if (this->get_type() == mad_type::INT) {
      rvar->set(mad_type::STR, (mad_STR_t) to_string((mad_INT_t) get<mad_INT_t>(val)));
    } else {
      fprintf(stderr, "Error: invalid type conversion to STR requested\n");
      return EXIT_FAILURE;
    }
  } else {
    fprintf(stderr, "Error: invalid conversion request type\n");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_type_t mad_var::get_type() {
  return type;
}

mad_val_t mad_var::get_val() {
  return val;
}

mad_status_t mad_var::get_prim_val(mad_val_t* rval, mad_type_t req_type) {
  if (type == mad_type::NONE) return EXIT_SUCCESS; //nothing to do
  if (!mad_type_is_primitive(req_type)) return EXIT_FAILURE;
  mad_var_t r;
  mad_status_t convert_status = convert(&r, req_type);
  if (convert_status != EXIT_SUCCESS) return convert_status;
  *rval = r.val;
  return EXIT_SUCCESS;
}

mad_status_t mad_var::get_string(string* ret_str) {
  mad_val_t rval = string("");
  if (get_prim_val(&rval, mad_type::STR) != EXIT_SUCCESS) return EXIT_FAILURE;
  *ret_str = get<mad_STR_t>(rval);
  return EXIT_SUCCESS;
}

mad_vars::mad_vars() {
}

// if lookup finds nothing, return EXIT_FAILURE
// otherwise *rvar will be set to the variable type/val found in the query and
// EXIT_SUCCESS returned
// note that this function will simply provide return with no set operation if
// provided rvar is NULL (useful if you only want to check for existence)
mad_status_t mad_vars::lookup(mad_var_t* rvar, string name) {
  auto query = all_vars.find(name);
  if (query == all_vars.end()) {
    return EXIT_FAILURE;
  }
  if (!rvar) return EXIT_SUCCESS;
  if (rvar->set(query->second.get_type(), query->second.get_val()) !=
      EXIT_SUCCESS) {
    fprintf(stderr, "Error: \"%s\" failed due to rvar set failure\n",
        __func__);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_status_t mad_vars::ins_var(string name, mad_var_t var, bool force) {
  if (name.length() > MAD_VAR_NAME_MAX) {
    fprintf(stderr, "Error: Var names must be shorter than %d characters!\n",
        MAD_VAR_NAME_MAX);
    return EXIT_FAILURE;
  }
  if (all_vars.find(name) == all_vars.end()) {
    if (all_vars.insert({name, var}).second) return EXIT_SUCCESS;
    fprintf(stderr, "Error: unable to insert variable into DB: %s\n",
        name.c_str());
    return EXIT_FAILURE;
  }
  if (!force) {
    fprintf(stderr, "Error: cannot re-assign existing variable \"%s\", try force assign instead\n",
        name.c_str());
    return EXIT_FAILURE;
  }
  // always succeeds, wether it is an insert or assign is irrelevant
  (void) all_vars.insert_or_assign(name, var);
  return EXIT_SUCCESS;
}

mad_status_t mad_vars::rm_var(string name) {
  if (!all_vars.erase(name)) {
    fprintf(stderr, "Error: unable to remove non-existent variable \"%s\"\n",
        name.c_str());
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_status_t mad_vars::print_vars(string filter) {
  for (int i = 0; i < 10; i++) printf("==========");
  printf("====");
  printf("\n");
  string fmt_str = "|%" + string(EVAL_MACRO_TO_STR(MAD_VAR_NAME_MAX)) +
    "s|%10s|%50s|\n";
  printf(fmt_str.c_str(), "VAR_NAME", "VAR_TYPE", "VAR_CUR_VALUE_AS_STR");
  for (int i = 0; i < 10; i++) printf("==========");
  printf("====");
  printf("\n");
  for(auto& n : all_vars) {
    string name = n.first;
    mad_var_t *var = &n.second;
    if (filter != "" && name.find(filter) == string::npos) continue;
    string val_str;
    if (var->get_string(&val_str) != EXIT_SUCCESS) {
      fprintf(stderr, "Warning: error getting variable \"%s\" as STR\n",
          name.c_str());
    }
    printf(fmt_str.c_str(),
        name.c_str(), mad_type_to_str(var->get_type()).c_str(),
        val_str.c_str());
  }
  for (int i = 0; i < 10; i++) printf("==========");
  printf("====");
  printf("\n");
  return EXIT_SUCCESS;
}
