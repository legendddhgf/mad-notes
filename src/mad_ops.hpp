#ifndef MAD_OPS_HPP
#define MAD_OPS_HPP

#include <cstdbool>
#include <unordered_map>

#include "common.hpp"
#include "mad_vars.hpp"

using std::string;
using std::unordered_map;

class mad_var;
class mad_vars;

typedef class mad_var mad_var_t;
typedef class mad_vars mad_vars_t;

typedef class mad_ops {
  private:
    class mad_vars* const rvars;
    unordered_map<string, mad_status_t (mad_ops::*)(string)> glbl_op_map;
    unordered_map<string, mad_status_t (*)(mad_var_t*, mad_var_t*, mad_var_t*)> var_op_map;
  public:
    mad_ops(mad_vars_t* ext_vars);
    mad_status_t process_line(string glbl_op, string args);
    mad_status_t var_op_query(string var_op);
    // Forwards to one of the below operations
    // write result into *rvar and return status
    mad_status_t exec(string var_op, mad_var_t* rvar, mad_var_t* x, mad_var_t* y);


    mad_status_t apply_val(mad_var_t* rvar, mad_type_t type, string args);
    mad_status_t generic_assign(string args, bool force);
    // GLOBAL OPS //
    /*
    mad_status_t help(mad_vars_t* rvars, string args);
    mad_status_t assign(mad_vars_t* rvars, string args);
    //force assign includes overwriting the variable if it exists
    mad_status_t fassign(mad_vars_t* rvars, string args);
    mad_status_t deassign(mad_vars_t* rvars, string args);
    mad_status_t listvars(mad_vars_t* rvars, string args);
    mad_status_t printvar(mad_vars_t* rvars, string args);
    */
    mad_status_t help(string args);
    mad_status_t assign(string args);
    //force assign includes overwriting the variable if it exists
    mad_status_t fassign(string args);
    mad_status_t deassign(string args);
    mad_status_t listvars(string args);
    mad_status_t printvar(string args);
    // END GLOBAL OPS //
} mad_ops_t;

typedef mad_status_t (mad_ops::*glbl_op_t)(string);

mad_status_t generic_scalar_op(mad_var_t* rvar, mad_var_t* x, mad_var_t* y,
    string var_op);

// MAD_VAR_T OPS //
// All mad_var_t ops write the result *x op *y into *rvar and return status
mad_status_t concat(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
mad_status_t sum(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
mad_status_t difference(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
mad_status_t product(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
mad_status_t quotient(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
mad_status_t remainder(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
// x^y
mad_status_t exponent(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
// log_x(y)
mad_status_t logarithm(mad_var_t* rvar, mad_var_t* x, mad_var_t* y);
// TODO trig functions?
// END MAD_VAR_T OPS //

#endif//MAD_OPS_HPP
