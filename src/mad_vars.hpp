#ifndef MAD_VARS_HPP
#define MAD_VARS_HPP

#include <cstdint>
#include <cstring>
#include <cstdbool>
#include <unordered_map>
#include <vector>
#include <variant>

#include "common.hpp"
#include "mad_val.hpp"
#include "mad_calc.hpp"

using std::string;
using std::unordered_map;
using std::vector;
using std::variant;

typedef variant<mad_FLT_t, mad_INT_t, mad_STR_t, mad_CALC_t> mad_val_t;

#define MAD_VAR_NAME_MAX 40

typedef class mad_var {
  private:
    mad_type_t type;
    mad_val_t val;
  public:
    mad_var();
    mad_var(mad_type_t init_type, mad_val_t init_val);
    mad_var(mad_type_t init_type, mad_val_t* pinit_val = NULL);
    mad_status_t set(mad_type_t set_type, mad_val_t set_val);
    mad_status_t set(mad_type_t set_type, mad_val_t* pset_val = NULL);
    // converting type to same type is a copy to rvar with success status
    // otherwise, current valid conversion is between:
    // INT to FLT (make decimal) and FLT to INT (take the floor of val.f)
    // and all types to STR
    // Note: doesn't convert in place, instead writes return to *rvar
    mad_status_t convert(class mad_var* rvar, mad_type_t req_type);
    mad_type_t get_type();
    mad_val_t get_val();
    //Effectively calculates a final primitive value (and performs conversion)
    //unlike get_val(), which simply returns the current val, which isn't always
    //what we want
    //writes return to *rval
    mad_status_t get_prim_val(mad_val_t* rval, mad_type_t req_type);
    mad_status_t get_string(string* ret_str);
} mad_var_t;

typedef class mad_vars {
  private:
    unordered_map<string, mad_var_t> all_vars;

  public:
    mad_vars();
    mad_status_t lookup(mad_var_t* rvar, string name);
    mad_status_t ins_var(string name, mad_var_t var, bool force = false);
    mad_status_t rm_var(string name);
    mad_status_t print_vars(string filter);
} mad_vars_t;

#endif//MAD_VARS_HPP
