#include "mad_val.hpp"

using std::variant_npos;

mad_type_t mad_val_to_mad_type(mad_val_t* rval) {
  if (!rval) return mad_type::NONE;
  switch(rval->index()) {
    case 0:
      return mad_type::FLT;
    case 1:
      return mad_type::INT;
    case 2:
      return mad_type::STR;
    case 3:
      return mad_type::CALC;
    case variant_npos:
    default:
      break;
  }
  return mad_type::NONE;
}
