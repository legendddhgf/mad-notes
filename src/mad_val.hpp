#ifndef MAD_VAL_HPP
#define MAD_VAL_HPP

#include <variant>

#include "common.hpp"
#include "mad_calc.hpp"

using std::variant;
using std::get;
using std::holds_alternative;

typedef variant<mad_FLT_t, mad_INT_t, mad_STR_t, mad_CALC_t> mad_val_t;

// provide &val as input
mad_type_t mad_val_to_mad_type(mad_val_t* rval);

#endif//MAD_VAL_HPP
