#include "mad_calc.hpp"

mad_calc::mad_calc(class mad_ops* rops, class mad_vars* rvars,
    vector<string> var_names, vector<string> op_names) {
  this->rops = rops;
  this->rvars = rvars;
  if (this->init(var_names, op_names) != EXIT_SUCCESS) {
    fprintf(stderr, "Warning: [1] CALC wasn't setup correctly!\n");
  }
}

mad_calc::mad_calc(class mad_ops* rops, class mad_vars* rvars, string args) {
  this->rops = rops;
  this->rvars = rvars;
  if (this->init(args) != EXIT_SUCCESS) {
    fprintf(stderr, "Warning: [2] CALC wasn't setup correctly!\n");
  }
}

mad_status_t mad_calc::init(vector<string> op_names, vector<string> var_names) {
  if (op_names.size() + 1 != var_names.size()) {
    fprintf(stderr, "Error: can't initialize CALC with unmatching list sizes\n");
    return EXIT_FAILURE;
  }
  //FIXME: check that vars all exist first
  this->var_names = var_names;
  this->op_names = op_names;
  return EXIT_SUCCESS;
}

// assume we have at least one var in CALC before
mad_status_t mad_calc::append(vector<string> op_names, vector<string> var_names) {
  if (op_names.size() != var_names.size()) {
    fprintf(stderr, "Error: can't append to CALC with unmatching list sizes\n");
    return EXIT_FAILURE;
  }
  size_t len = var_names.size();
  if (!len) return EXIT_SUCCESS; // nothing to do
  this->op_names.insert(this->op_names.end(), op_names.begin(), op_names.end());
  this->var_names.insert(this->var_names.end(), var_names.begin(), var_names.end());
  return EXIT_SUCCESS;
}

#define PARSE_STATE_VAR false
#define PARSE_STATE_OP true
mad_status_t mad_calc::init(string args) {
  if (args.length() == 0) return EXIT_SUCCESS;
  string targs = args;
  bool parse_state = PARSE_STATE_VAR; // false: expecting var/const, expecting op
  while (targs.size()) {
    size_t cur_str_len = targs.find_first_of(' ');
    string cur_str;
    if (cur_str_len == string::npos) {
      cur_str_len = targs.size();
    }
    cur_str = targs.substr(0, cur_str_len);
    // TODO: handle constants
    if (parse_state == PARSE_STATE_VAR) {
      if ((rvars->lookup(NULL, cur_str)) != EXIT_SUCCESS) {
        fprintf(stderr, "Error: CALC init failed due to reliance on non-existent variable: ");
        fprintf(stderr, "%s\n", cur_str.c_str());
        return EXIT_FAILURE;
      }
      var_names.push_back(cur_str);
    } else { // parse_state == PARSE_STATE_OP
      if ((rops->var_op_query(cur_str)) != EXIT_SUCCESS) {
        fprintf(stderr, "Error: CALC init failed due to reliance on non-existent op: ");
        fprintf(stderr, "%s\n", cur_str.c_str());
        return EXIT_FAILURE;
      }
      op_names.push_back(cur_str);
    }
    if (cur_str_len == targs.size()) break;
    parse_state = !parse_state;
    targs = trim_ws(targs.substr(cur_str_len + 1, targs.size() - cur_str_len - 1));
  }
  if (op_names.size() + 1 != var_names.size()) {
    fprintf(stderr, "Error: CALC created with incorrect number of (ops=%lu,vars=%lu)\n",
        op_names.size(), var_names.size());
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

mad_status_t mad_calc::eval(mad_var_t* rvar, mad_type_t req_type) {
  if (!mad_type_is_primitive(req_type)) return EXIT_FAILURE;
  if (var_names.size() != 0 && var_names.size() != op_names.size() + 1) {
    fprintf(stderr, "Error: malformed CALC var!\n");
    return EXIT_FAILURE;
  }
  mad_status_t ret;
  bool processed_first_var = false;
  mad_var_t x;
  mad_var_t y(req_type); // initialize in case CALC is empty
  for (size_t iter = 0; iter < this->var_names.size(); iter++) {
    string op_name;
    if (processed_first_var) { // op lookup only after the first var
      op_name = op_names[iter - 1];
    }
    // var lookup shouldn't fail at this point (shouldn't be constructed with
    // non-existent variable names)
    if ((ret = rvars->lookup(&y, var_names[iter])) != EXIT_SUCCESS) {
      fprintf(stderr, "Error: CALC eval failed due to reliance on var that doesn't exist: %s\n",
          this->var_names[iter].c_str());
      return ret;
    }
    if (!processed_first_var) { // no op until 1st op/2nd var are retrieved
      if ((ret = x.set(y.get_type(), y.get_val())) != EXIT_SUCCESS) {
        fprintf(stderr, "Error: CALC eval failed due to a set error\n");
        return ret;
      }
      processed_first_var = true;
      continue;
    }
    // Note: conversions to the appropriate primitive values are all done
    // automatically here
    // order is also important here since X has first arg (current accumulated
    // value) and y is second arg (recently retrieved variable)
    if ((ret = rops->exec(op_names[iter - 1], &y, &x, &y)) != EXIT_SUCCESS) {
      fprintf(stderr, "Error: CALC eval failed due to an OP failure\n");
      return ret;
    }
    if ((ret = x.set(y.get_type(), y.get_val())) != EXIT_SUCCESS) {
      fprintf(stderr, "Error: CALC eval failed due to a set error\n");
      return ret;
    }
  }
  // invariant: maintain that y has the final value when it exits the above loop
  string set_str;
  y.get_string(&set_str);
  if ((ret = rvar->set(mad_type::STR, set_str)) != EXIT_SUCCESS) {
      fprintf(stderr, "Error: CALC eval failed due to the final set() failing\n");
      return ret;
  }
  return EXIT_SUCCESS;
}

