#ifndef COMMON_HPP
#define COMMON_HPP

#include <algorithm>
#include <string>
#include <cstdbool>
#include <cstdarg>

#define SMALL_BUF_LEN 1024
#define MAX_LINE_LEN SMALL_BUF_LEN

#define _EVAL_MACRO_TO_STR(MACRO_EVAL) #MACRO_EVAL
#define EVAL_MACRO_TO_STR(MACRO) _EVAL_MACRO_TO_STR(MACRO)

using std::min;
using std::max;
using std::string;


typedef int mad_status_t;
typedef double mad_FLT_t;
typedef long mad_INT_t;
typedef string mad_STR_t;

// Floating point (double), Integer (Long), or String (string) respectively
// CALC is a special type wherein the value is computed from other mad_var_t's
typedef enum class mad_type {
  FLT, INT, STR, //primitives
  CALC, //special
  NONE //type not found
} mad_type_t;

void set_verbose();
void clr_verbose();
void info(string fmt...);
void vinfo(string fmt...);
void warn(string fmt...);
void error(string fmt...);

string trim_ws(string);

bool mad_type_is_primitive(mad_type_t type);
bool mad_type_is_scalar_primitive(mad_type_t type);
string mad_type_to_str(mad_type_t type);
mad_type_t str_to_mad_type(string str);

#endif//COMMON_HPP
