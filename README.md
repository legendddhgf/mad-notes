# MAD-notes

Math-Analytics-Database (MAD) note taking system. Basically allows you to define variables to values or to the results of mathematical operations of existing variables

# Compile and Run

```
   make
   ./mad_notes
```
